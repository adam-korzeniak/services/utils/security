package com.adamkorzeniak.utils.security.infrastructure.model;

import lombok.Data;
import lombok.Generated;
import lombok.ToString;

@Data
@Generated
public class User {
    private Long id;
    private String username;
    @ToString.Exclude
    private String password;
    private Role role;
    private boolean enabled;
}
