package com.adamkorzeniak.utils.security.infrastructure.persistence;

import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import com.adamkorzeniak.utils.security.test.utils.TestData;
import com.adamkorzeniak.utils.security.test.utils.builder.UserBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureTestEntityManager
@Transactional
@SpringBootTest
@ActiveProfiles(profiles = "test")
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setup() {
        UserEntity adminUser = UserBuilder.sample().build();
        entityManager.persist(adminUser);
    }

    @AfterEach
    void tearDown() {
        entityManager.clear();
    }

    @Test
    void FindByUsername() {
        assertThat(userRepository.findByUsername(TestData.USER_USERNAME)).isEmpty();
        assertThat(userRepository.findByUsername(TestData.ADMIN_USERNAME)).isPresent();
    }

    @Test
    void ExistsByUsername() {
        assertThat(userRepository.existsByUsername(TestData.USER_USERNAME)).isFalse();
        assertThat(userRepository.existsByUsername(TestData.ADMIN_USERNAME)).isTrue();
    }
}
