package com.adamkorzeniak.utils.security.test.utils.builder;

import com.adamkorzeniak.utils.security.infrastructure.model.Role;
import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import com.adamkorzeniak.utils.security.test.utils.TestData;

public class UserBuilder {
    private String username;
    private String password;
    private Role role;
    private boolean enabled;

    public static UserBuilder sample() {
        UserBuilder userBuilder = new UserBuilder();
        userBuilder.username = TestData.ADMIN_USERNAME;
        userBuilder.password = TestData.PASSWORD;
        userBuilder.role = Role.ADMIN;
        userBuilder.enabled = true;
        return userBuilder;
    }

    public UserBuilder password(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public UserEntity build() {
        UserEntity user = new UserEntity();
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        user.setEnabled(enabled);
        return user;
    }

}
